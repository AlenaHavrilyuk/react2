import React from 'react';
import { connect } from 'react-redux';
import './messageInput.css';
import idCreator from '../utils/idCreator';
import currentUserId from '../users/currentUser';
import { addMessage } from '../chat/actions';
import { setInputValue } from '../messageInput/actions'

class MessageInput extends React.Component{
    constructor(props){
        super(props); 
        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);    
    }
    onSubmit(event) {
        const newMassage = {
            id: idCreator(),
            userId: currentUserId,
            avatar: "",
            user: "Me",
            text: this.props.input.value,
            createdAt: new Date().toISOString(),
            editedAt: ""
        }
        this.props.addMessage(newMassage);
        this.props.setInputValue("");
        event.preventDefault();
    }

    handleChange(event) {
        this.props.setInputValue(event.target.value);
    }

    render(){
        return(
        <form className="message-input" onSubmit={this.onSubmit}>
            <input type="text" value={this.props.input.value} onChange={this.handleChange}></input>
            <button type="submit">Send</button>
        </form>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        input: state.input
    }
};

const mapDispatchToProps = {
    setInputValue,
    addMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);