import { EDIT_TEXT } from "./actionType";

const initialState = {
    text: ""
}

export default function (state=initialState, action){
    switch(action.type) {
        case EDIT_TEXT: {
            return Object.assign({}, state, {
                text: action.text
            })
        }
        default:
            return state;
    }
    
}