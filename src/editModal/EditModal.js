import React from 'react';
import { connect } from 'react-redux';
import './editModal.css';
import { saveEditedMessage, closeEditModal } from '../chat/actions'
import { editText } from '../editModal/actions'


class EditModal extends React.Component{
    constructor(props){
        super(props); 
        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onClose = this.onClose.bind(this);    
    }
    onSubmit(event) {   
        this.props.saveEditedMessage(this.props.editedMessage.text);
        event.preventDefault();
    }

    onClose(event){
        this.props.closeEditModal();
        event.preventDefault();
    }

    handleChange(event) {
        this.props.editText(event.target.value);
    }

    render(){
        let editModal = this.props.editModal;
        let value = this.props.editedMessage ? this.props.editedMessage.text : "";
        return(
        <form className={editModal ?  "edit-message-modal modal-shown" : "edit-message-modal"} >
            <textarea className="edit-message-input" value={value} onChange={this.handleChange}/>
            <button className="edit-message-button" onClick={this.onSubmit} >save</button>
            <button className="edit-message-close" onClick={this.onClose}>close</button>
        </form>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        editedMessage: state.editedMessage
    }
  };
  
const mapDispatchToProps = {
    editText,
    saveEditedMessage,
    closeEditModal
}
  
export default connect(mapStateToProps, mapDispatchToProps)(EditModal);