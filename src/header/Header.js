import React from 'react';
import './header.css';
import { format, parseISO } from 'date-fns';

class Header extends React.Component{
    render(){    
        let date = format(parseISO(this.props.lastMessageDate), 'dd.MM.yyyy kk:mm');
        return(
        <div className="header">
            <p className="header-title">
            My chat
            </p>
            <p className="header-users-count">
            {this.props.usersCount} participants
            </p>
            <p className="header-messages-count">
            {this.props.messagesCount} messages
            </p>
            <p className="header-last-message-date">
            last message at: {date}
            </p>
        </div>
        )
    }
}

export default Header;