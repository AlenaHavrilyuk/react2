import React from 'react';
import preloader from '../images/preloader.gif'
import './preloader.css'

class Preloader extends React.Component {
    render() {
        return (
            <div className="preloader">
                <img className="spinner" src={preloader} />
            </div>
        );
    }
}

export default Preloader;

