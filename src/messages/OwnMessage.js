import React from 'react';
import { connect } from 'react-redux';
import { format } from 'date-fns';
import {deleteMessage, callEditModal} from '../chat/actions';
import { editText } from '../editModal/actions'

class OwnMessage extends React.Component{
    constructor(props){
        super(props); 
        this.editMessage = this.editMessage.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);         
    }

    editMessage(){
        this.props.editText(this.props.message.text)
        this.props.callEditModal();
    }

    deleteMessage(){
        this.props.deleteMessage(this.props.message.id)
    }

    render(){
        let data = this.props.message;
        let edited = "";
        let datetime = new Date(data.createdAt)
        if(data.editedAt !== "") {
            edited = "edited";
        }
        let time = format(datetime, 'kk:mm');
        let canBeEdited = this.props.canBeEdited;
        return(
        <div className={canBeEdited ? "own-message can-be-edited" : "own-message"}>
            <div className="message-text">
                {data.text}
            </div>
            <div className="message-time">
                {time} {edited}
            </div>
            <button className="message-edit" onClick={this.editMessage}>
                edit
            </button>
            <button className="message-delete" onClick={this.deleteMessage}>
                delete
            </button>
            <div className="message-background"></div>
        </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        chat: state.chat
    }
  };
  
const mapDispatchToProps = {
    editText,
    deleteMessage,
    callEditModal
}
  
export default connect(mapStateToProps, mapDispatchToProps)(OwnMessage);
