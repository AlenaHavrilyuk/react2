import React from 'react';

class LikeButton extends React.Component{
    constructor(props){
        super(props); 
        this.state = {isLiked: false};
        this.likeMessage = this.likeMessage.bind(this);         
    }
    likeMessage() {
        this.setState(prevState => ({
            isLiked: !prevState.isLiked
        }));
    }


    render(){
        if (!this.state.isLiked) {
            return(
                <button onClick={this.likeMessage} className="message-like">
                    ♡
                </button>
            )
        } else {
            return(
                <button onClick={this.likeMessage} className="message-liked">
                    ❤
                </button>
            )
        }
        
    }      
}

export default LikeButton;