import React from 'react';
import { connect } from 'react-redux';
import { isAfter, isSameDay, startOfYear, subDays, format, parseISO } from 'date-fns';
import Message from './Message';
import './messageList.css'
import OwnMessage from './OwnMessage';
import Divider from './Divider';
import currentUserId from '../users/currentUser';

class MessageList extends React.Component {

  messagesEndRef = React.createRef()

  componentDidMount () {
    this.scrollToBottom();
  }
  componentDidUpdate () {
    this.scrollToBottom()
  }
  scrollToBottom = () => {
    this.messagesEndRef.current.scrollIntoView({ behavior: 'smooth' })
  }

  formatDate = (dateToBeFormatted) => {
    const today = new Date();
    const yesterday = subDays(today, 1);
    const startOfThisYear = startOfYear(today);
    const date = parseISO(dateToBeFormatted);

    let result = "";
    if (isSameDay(date, today)) {
      result = "today";
    } else if (isSameDay(date, yesterday)) {
      result = "yesterday";
    } else if (isAfter(date, startOfThisYear)) {
      result = format(date, 'eeee, MMMM do'); 
    } else {
      result = format(date, 'eeee, MMMM do yyyy'); 
    }

    return result;
  }

  render() {
    const msgList = [];
    let prevMessageDate = null;
    let lastOwnMessageId;
    for (let index = this.props.messages.length - 1; index >= 0; index--) {
      if (this.props.messages[index].userId === currentUserId) {
        lastOwnMessageId = this.props.messages[index].id;
        break;
      }
    }

    this.props.messages.forEach((msg) => {
      let date = this.formatDate(msg.createdAt);

      if (date !== prevMessageDate) {
        msgList.push(
          <Divider key={"_" + date} date={date} />
        );
      };
      prevMessageDate = date;

      if (msg.id === lastOwnMessageId) {
        msgList.push(
          <OwnMessage
            key={msg.id}
            message={msg}
            canBeEdited={true}
          />
        );
      } else if (msg.userId === currentUserId) {
        msgList.push(
          <OwnMessage
            key={msg.id}
            message={msg}
            canBeEdited={false}
          />
        );
      } else {
        msgList.push(
          <Message
            key={msg.id}
            message={msg}
          />
        );
      }
    })

    return (
      <div className="message-list">
        {msgList}
        <div className="messages-end" ref={this.messagesEndRef} />
      </div>
    )

  }
}

export default MessageList;