import React from 'react';

class Divider extends React.Component{
    
    render(){
        return(
            <div className="messages-divider">
                {this.props.date}
            </div>
        )
    }
        
}      


export default Divider;