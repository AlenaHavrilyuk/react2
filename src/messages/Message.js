import React from 'react';
import './message.css'
import LikeButton from './likeButton';
import { format } from 'date-fns';

class Message extends React.Component{
    render(){
        let data = this.props.message;
        let edited = "";
        let datetime;
        if(data.editedAt === "") {
            datetime = new Date(data.createdAt);
        } else{
            datetime = new Date(data.editedAt);
            edited = "edited";
        }
        let time = format(datetime, 'kk:mm');
        return(
        <div className="message">
            <div className="message-text">
                {data.text}
            </div>
            <div className="message-time">
                {time} {edited}
            </div>
            <LikeButton />
            <div className="message-user-name">
                {data.user}
            </div>
            <div className="message-user-avatar">
                <img src={data.avatar} alt="../images/no-avatar.png"/>
            </div>
            <div className="message-background"></div>
        </div>
        )
    }
}

export default Message;