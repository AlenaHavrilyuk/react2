import currentUserId from '../users/currentUser';
import { SET_LOADED_DATA, ADD_MESSAGE, DELETE_MESSAGE, CALL_EDIT_MODAL, SAVE_EDITED_MESSAGE, CLOSE_EDIT_MODAL } from './actionType';

const initialState = {
    messages: [],
    editModal: false,
    preloader: true
}

export default function (state=initialState, action){
    switch(action.type) {
        case SET_LOADED_DATA: {
            return Object.assign({}, state, {
                messages: action.data,
                editModal: false,
                preloader: false
                
            })
        }
        case ADD_MESSAGE: { 
            return Object.assign({}, state, {
                messages:  [
                    ...state.messages,
                    action.message
                ],
                editModal: false,
                preloader: false
            })
        }
        case DELETE_MESSAGE: {
            return Object.assign({}, state, {
                messages:  state.messages.filter((message) => message.id !== action.messageId),
                editModal: false,
                preloader: false
            })
        }

        case CALL_EDIT_MODAL: {
            let lastOwnMessageText;
            for (let index = state.messages.length - 1; index >= 0; index--) {
                if (state.messages[index].UserId = currentUserId) {
                    lastOwnMessageText = state.messages[index].text;
                    break;
                }
            }
            return Object.assign({}, state, {
                messages:  state.messages,
                editModal: true,
                preloader: false
            })
        }

        case SAVE_EDITED_MESSAGE: {
            let lastOwnMessageIndex;
            for (let index = state.messages.length - 1; index >= 0; index--) {
                if (state.messages[index].userId === currentUserId) {
                    lastOwnMessageIndex = index;
                    break;
                }
            }
            let updatedMessages = state.messages;
            updatedMessages[lastOwnMessageIndex].text = action.text;
            updatedMessages[lastOwnMessageIndex].editedAt = new Date().toISOString();
            return Object.assign({}, state, {
                messages:  updatedMessages,
                editModal: false,
                preloader: false
            })

        }
        case CLOSE_EDIT_MODAL : {
            return Object.assign({}, state, {
                messages:  state.messages,
                editModal: false,
                preloader: false
            })
        }

        default:
            return state;
    }
}