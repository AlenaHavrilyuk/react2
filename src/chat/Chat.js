import React from 'react';
import { connect } from 'react-redux';
import Header from '../header/Header';
import MessageList from '../messages/MessageList';
import Preloader from '../preloader/Preloader';
import EditModal from '../editModal/EditModal';
import './chat.css'
import MessageInput from '../messageInput/MessageInput';
import { setLoadedData } from './actions';


class Chat extends React.Component {

    componentDidMount() {
        fetch(this.props.url)
        .then(response => response.json())
        .then(data => {this.props.setLoadedData(data)})
    }

    render(){
        if(this.props.chat.preloader){
            return (
                <div className="chat">
                    <Preloader />
                </div>
            )
        }
        else{  
            let data = this.props.chat.messages;
            let messCount = data.length;
            let userSet = new Set();
            data.forEach(function(item) {
                userSet.add(item.userId);
            });
            let userCount = userSet.size;
            let lastMessDate = data[data.length - 1].createdAt;

            return (
                <div className="chat">
                    <Header messagesCount={messCount} usersCount={userCount} lastMessageDate={lastMessDate}/>
                    <MessageList messages={data}/> 
                    <MessageInput />
                    <EditModal editModal={this.props.chat.editModal}/>
                </div>
            )
        }   
    }
}

const mapStateToProps = (state) => {
    return {
        chat: state.chat
    }
};

const mapDispatchToProps = {
    setLoadedData
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);