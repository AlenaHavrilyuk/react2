import { combineReducers } from "redux";
import chat from '../chat/reducer'
import input from '../messageInput/reducer';
import editedMessage from '../editModal/reducer';

const rootReducer = combineReducers(
    {
        chat,
        input,
        editedMessage
    }
);

export default rootReducer;